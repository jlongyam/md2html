#!/bin/bash
sudo apt install pandoc

echo 'Installation check..'
current_loc=$PWD
echo "$current_loc"
md_bin="$current_loc/bin"
echo $md_bin
echo 'set bin to PATH..'
export PATH=$PATH:$md_bin
echo $PATH
cd test
echo 'try convert test/README.md ..'
md README.md
firefox --new-window "file://$PWD/README.md.html"
echo 'Installation success ..'
exit
