# md2html

[gitlab.com/jlongyam/md2html](https://gitlab.com/jlongyam/md2html)

## Istallation

Note: this app require `pandoc`

### Manually

####1. create folder `Program` (optional). put all folder and file in it, except `.git`
####2. add `md/bin` path to .bashrc

```shell
PATH=$PATH:"/path/to/md/bin"
export PATH
```

####3. point source, type in terminal

`source ./.bashrc`

####4. goto bin folder, try

```shell
./md ../test/README.md
```

### Using Installer

Current installer work on ubuntu OS family.

It's not permanent Install, just for test.

Use manual install for work.

### Usage

After success install it can run like

```shell
md ./README.md

output "./README.md.html"
```
It's mean installation success

Touched now and now

