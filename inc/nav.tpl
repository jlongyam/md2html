<script>
const toggle_hidden = (ele) => (ele.hidden = !ele.hidden);
document.addEventListener('DOMContentLoaded', ( event ) => {
  var a = document.links, i = 0
  for( i; i< a.length;i++) {
    contain_hash = ( a[i].href.indexOf( '#' ) != -1 )
    if( contain_hash ) {
      s_link = a[i].href
      s_link_before = s_link.substring( 0, s_link.indexOf('#') )
      i_pos = s_link.indexOf( '#' ) + 1
      s_link_after = s_link.substring( i_pos );
      s_link_new = "o_" + s_link_after
      a[i].href = s_link_before+'#'+s_link_new
    }
  }
  function create_nav() {
    var fargment = document.createDocumentFragment()
    var nav = document.createElement('nav')
    var el = document.querySelectorAll('[id*="o_"]')
    var s_html = '<ul>'
    for( var i = 0; i< el.length; i++) {
      // console.log(el[i].tagName)
      var s_content = '<a href="#' + el[i].id + '">'+el[i].textContent+'</a>'
      if( el[i].tagName == 'H1') {
        s_html += '<li>' + s_content + '</li>'
      }
      if( el[i].tagName == 'H2' ) {
        s_html += "<ul>"
        s_content = '<li>' + s_content + '</li>'
        s_html += s_content
        s_html += "</ul>"
      }
      if( el[i].tagName == 'H3' ) {
        s_html += "<ul>"
        s_content = '<li><ul><li>' + s_content + '</li></ul></li>'
        s_html += s_content
        s_html += "</ul>"
      }
    }
    s_html += '</ul>'
    nav.innerHTML = s_html
    nav.setAttribute('hidden','')
    fargment.appendChild( nav )
    var button_nav = document.createElement('button')
    button_nav.textContent = '☰'
    fargment.appendChild( button_nav )
    document.body.appendChild(fargment)
    nav.id = '_nav'
    button_nav.id = '_nav_button'
    var styleString = `
      #_nav {
        position: fixed;
        right: 0;
        top: 0;
        bottom: 0;
        width: 300px;
        background-color: white;
        overflow: auto;
        padding: 30px 10px;
        border-left: 1px solid gray;
        white-space: nowrap;
      }
      #_nav ul {
        margin: 0;
        padding: 0;
        }
      #_nav ul li {
        margin: 0;
        padding-left: 20px;
        }
      #_nav_button {
        position: fixed;
        right: 20px;
        top: 20px;
        border: 2px solid gray;
        padding: 10px;
        background-color: silver;
        cursor: pointer;
        font-weight: bold;
        z-index: 90;
      }
    `
    var style = document.createElement( 'style')
    document.head.appendChild(style)
    style.textContent = styleString;

    button_nav.addEventListener('click', ()=> {
      toggle_hidden( nav )
    })
    nav.addEventListener('click',()=> {
      toggle_hidden( nav )
    })
  }
  create_nav()
})
</script>